#include <string>
#include <iostream>

#include "node.h"

// fortran record delimiter length
#define RECORD_DELIMITER_LENGTH 4


/* Load the ramses output into an octree. Return the root. */
Node* load_ramses(std::string output_dir, int max_lvl);

/* Deallocate the tree */
void cleanup(Node* root);


/* ---------- Helper functions ----------------*/

/* Convert 3D array index to 1D array index */
int d3_to_d1(int ix, int iy, int iz);

/* Convert 1D array index to 3D array index */
void d1_to_d3(int ind, int (&index)[3]);


/* ---------Functions for reading fortran --------------*/

/* Construct the file name for a given cpu, output directory and species */
std::string construct_name(int icpu, std::string output_dir, std::string kind);

/* Read a record from a fortran binary output file */
void readInt(std::ifstream& input, int& value);

/* Skip a fortran record */
void skipRecord(std::ifstream& input);

/* Process the header of a ramses output file */
void process_header(std::ifstream& input, int& ncpu, int& ndim, int& nx, int& ny, int& nz,
                  int& nlevelmax, int& ngridmax,int& nboundary, int& ngrid_current, double& boxlen);


/*-----------Functions for writing the octree --------------*/

/* Recursively write the octtree */
void write_node(Node* node, int lvl, int max_lvl, std::ofstream& output);

/* Write the entire grid to txt file */
void write_grid(Node* root, int max_lvl, std::string filename);

