/* class to represent a tree node */
class Node {

    private:
        int lvl;               // refinement level
        Node* parent;          // parent node
        Node* childeren[8];    // childeren  nodes
        double rho;            // density
        double pos[3];         // position

    public:

        // constructor
        Node(Node* par) {
            parent = par;
            for (int s=0;s<8;s++) {
                childeren[s] = NULL;
            }
            rho=0;
        }

        //destructor
        ~Node() {
            for (int s=0;s<8;s++){
                if(childeren[s]!=NULL)
                    delete childeren[s];
            }
        }

        // Setters
        void setRho(double value) {
            rho = value;
        }
        void setLvl(int value){
            lvl = value;
        }
        void setPos(double x, double y, double z){
            pos[0] = x;
            pos[1] = y;
            pos[2] = z;
        }

        // Getters
        double getRho() {
            return rho;
        }
        double getX(){
            return pos[0];
        }
        double getY(){
            return pos[1];
        }
        double getZ(){
            return pos[2];
        }
        Node* getParent() {
            return parent;
        }
        Node* getChild(int index) {
            return childeren[index];
        }

        //add a child node
        void addSon(Node* child, int index) {
            childeren[index] = child;
        }

        // returns if the node has any childeren
        bool isRefined(){
            bool ref = false;
            int s=0;
            while (s<8 && ref==false) {
                if (childeren[s]!=NULL) {
                    ref=true;
                }
                s++;
            }
            return ref;
        }
};
