#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <stdlib.h>
#include <string>
#include <cmath>
#include <vector>

#include "readAMR.h"

using namespace std;

/* Convert 3D array index to 1D array index */
int d3_to_d1(int ix, int iy, int iz) {
    return 4*iz + 2*iy + ix;
}

/* Convert 1D array index to 3D array index */
void d1_to_d3(int ind, int (&index)[3]) {
    index[2] = ind/4;
    index[1] = (ind-4*index[2])/2;
    index[0] = (ind-2*index[1]-4*index[2]);
}

/* Construct the file name for a given cpu, output directory and species */
string construct_name(int icpu, string output_dir, string kind){
    int ipos = output_dir.find("output_");
    string nchar = output_dir.substr(ipos+7,5);
    string icpu_string;
    ostringstream convert;
    convert << (icpu+1);
    icpu_string = convert.str();
    string cpu_number;
    if((icpu+1)>=10000){
        cpu_number = ".out" + icpu_string;
    } else if ((icpu+1)>=1000) {
        cpu_number = ".out0" + icpu_string;
    } else if ((icpu+1)>=100) {
        cpu_number = ".out00" + icpu_string;
    } else if ((icpu+1)>=10) {
        cpu_number = ".out000" + icpu_string;
    } else {
        cpu_number = ".out0000" + icpu_string;
    }
    string name_file = output_dir + "/" + kind + "_" + nchar + cpu_number;
    return name_file;
}

/* Read a record from a fortran binary output file */
void readInt(ifstream& input, int& value) {
    // skip record length at beginning of fortran record
    int record_size=0;
    input.read((char*) &record_size, RECORD_DELIMITER_LENGTH);
    // read and store the value
    input.read((char*) &value, record_size);
    // skip record length at end of fortran record
    input.seekg(RECORD_DELIMITER_LENGTH, ios::cur);
}

/* Skip a fortran record */
void skipRecord(ifstream& input){
    int record_size=0;
    input.read((char*) &record_size, RECORD_DELIMITER_LENGTH);
    input.ignore(record_size);
    input.seekg(RECORD_DELIMITER_LENGTH, ios::cur);
}

/* Recursively write a node of the octtree */
void write_node(Node* node, int lvl, int max_lvl, ofstream& output) {
    //int cur_lvl=lvl;
    Node* current_node = NULL;
    output << "! 2 2 2\n";
    for (int ix=0;ix<2;ix++) {
        for (int iy=0;iy<2;iy++) {
            for (int iz=0;iz<2;iz++) {
                // get the child with ID determined by indices ix iy iz
                current_node = node->getChild(d3_to_d1(ix,iy,iz));
                if((lvl<max_lvl) && current_node->isRefined()){
                    //go further in ref
                    write_node(current_node, lvl+1, max_lvl, output);
                } else {
                    //output the node: level density
                    output << lvl << ' ' << current_node->getRho() << '\n';
                }
            }
        }
    }
}

/* Write the entire grid until a certain lvl */
void write_grid(Node* root, int max_lvl, string filename){
    ofstream output;
    output.open(filename.c_str());

    write_node(root, 1, max_lvl, output);

    output.close();

    cout << "Tree written to " << filename << "\n";
}

/* Process the header of a ramses output file */
void process_header(ifstream& input, int& ncpu, int& ndim, int& nx, int& ny, int& nz,
                    int& nlevelmax, int& ngridmax,int& nboundary, int& ngrid_current, double& boxlen) {
    readInt(input, ncpu);
    cout << "ncpu " << ncpu << '\n';
    readInt(input, ndim);
    cout << "ndim " << ndim << '\n';
    input.seekg(RECORD_DELIMITER_LENGTH, ios::cur);
    input.read((char*) &nx, sizeof(int));
    input.read((char*) &ny, sizeof(int));
    input.read((char*) &nz, sizeof(int));
    input.seekg(RECORD_DELIMITER_LENGTH, ios::cur);
    cout << "nx " << nx << " ny " << ny << " nz " << nz << '\n';
    readInt(input, nlevelmax);
    cout << "nlevelmax " << nlevelmax << '\n';
    readInt(input, ngridmax);
    cout << "ngridmax " << ngridmax << '\n';
    readInt(input, nboundary);
    cout << "nboudnary " << nboundary << '\n';
    readInt(input, ngrid_current);
    cout << "ngrid_current " << ngrid_current << '\n';
    input.seekg(RECORD_DELIMITER_LENGTH, ios::cur);
    input.read((char*) &boxlen, sizeof(double));
    input.seekg(RECORD_DELIMITER_LENGTH, ios::cur);
    cout << "boxlen " << boxlen << '\n';
}

/* Load the ramses output into an octree. Return the root. */
Node* load_ramses(string output_dir, int max_lvl) {

    double xc[8][3];

    // determine name of the first amr file
    string name_amr_file1 = construct_name(0, output_dir, "amr");
    ifstream file_amr1;
    file_amr1.open(name_amr_file1.c_str(), ios::in | ios::binary);
    // Check if the output files exist
    if (!file_amr1.is_open()) {
        cout << "Could not open the adaptive mesh data " << name_amr_file1;
    }
    cout << "Reading header from " << name_amr_file1 << '\n';

    // READ HEADER INFO (valid for all cpus)
    int ncpu, ndim, nx, ny, nz, nlevelmax, ngridmax, nboundary, ngrid_current;
    double boxlen;
    process_header(file_amr1, ncpu, ndim, nx, ny, nz, nlevelmax, ngridmax, nboundary, ngrid_current, boxlen);
    int twotondim=int(pow(2,ndim)); //oct contains 8 cells
    double xbound[3] ={double(nx)/2,double(ny)/2,double(nz)/2};

    // Allocate ngridfile
    int** ngridfile = new int*[ncpu+nboundary];
    for (int a=0;a<(ncpu+nboundary);a++){
        ngridfile[a] = new int[nlevelmax];
        for (int l=0;l<nlevelmax;l++){
            ngridfile[a][l] = 0;
        }
    }

    // skip header: time variables
    for (int i=0;i<11;i++) {
        skipRecord(file_amr1);
    }
    // skip header: level variables (headl, taill)
    for (int i=0;i<2;i++) {
        skipRecord(file_amr1);
    }
    // read header: level variables (numbl) = grid numbers
    file_amr1.seekg(RECORD_DELIMITER_LENGTH, ios::cur);
    for (int l=0; l<nlevelmax;l++) {
        for (int i=0;i<ncpu;i++){
            file_amr1.read((char*)&(ngridfile[i][l]), sizeof(int));
        }
    }
    file_amr1.seekg(RECORD_DELIMITER_LENGTH, ios::cur);
    file_amr1.close();

    // HYDRO HEADER
    // open hydro file
    string name_hydro_file1 = construct_name(0, output_dir, "hydro");
    ifstream file_hydro1;
    file_hydro1.open(name_hydro_file1.c_str(), ios::in | ios::binary);
    if (!file_hydro1.is_open()) {
        cout << "Could not open the hydro data " << name_hydro_file1;
        return NULL;
    }
    // skip header
    skipRecord(file_hydro1); //ncpu
    //nvar
    int nvarh;
    readInt(file_hydro1, nvarh);
    cout << "nvarh " << nvarh << '\n';
    file_hydro1.close();

    // ROOT of the octree
    Node* root = new Node(NULL);
    root->setLvl(1);
    root->setRho(0);
    root->setPos(0.5,0.5,0.5);

    Node* current_parent = root;

    //loop over octree levels
    for (int tree_lvl=0;tree_lvl<max_lvl; tree_lvl++) {
        cout << "TREE LVL " << tree_lvl+1 << "\n";
        // Loop over processor files
        for (int icpu=0; icpu<ncpu; icpu++) {

            // AMR FILE

            // Open AMR file
            string name_amr_file = construct_name(icpu, output_dir, "amr");
            //cout << name_amr_file << "\n";
            ifstream file_amr;
            file_amr.open(name_amr_file.c_str(), ios::in | ios::binary);
            if (!file_amr.is_open()) {
                cout << "Could not open the adaptive mesh data " << name_amr_file;
                return NULL;
            }

            // Deal with header
            // skip: grid variables (8), time variables (11), level variables (headl, taill)
            for (int i=0;i<21;i++) {
                skipRecord(file_amr);
            }
            // read: level variables (numbl) = grid numbers
            // REMARK: carefull with column row stuff!
            //         Fortran writes 2D arrays column per column
            file_amr.seekg(RECORD_DELIMITER_LENGTH, ios::cur);
            for (int l=0; l<nlevelmax;l++) {
                for (int i=0;i<ncpu;i++){
                    file_amr.read((char*)&(ngridfile[i][l]), sizeof(int));
                }
            } // order seems OK
            file_amr.seekg(RECORD_DELIMITER_LENGTH, ios::cur);
            // skip something
            skipRecord(file_amr);
            // read boundary linked list
            if (nboundary>0){
                for (int i=0;i<2;i++){
                    skipRecord(file_amr);
                }
                file_amr.seekg(RECORD_DELIMITER_LENGTH, ios::cur);
                for (int l=0; l<nlevelmax;l++) {
                    for (int i=ncpu;i<(ncpu+nboundary);i++){
                        file_amr.read((char*)&(ngridfile[i][l]), sizeof(int));
                    }
                }
                file_amr.seekg(RECORD_DELIMITER_LENGTH, ios::cur);
            }
            // skip free memory
            skipRecord(file_amr);
            skipRecord(file_amr);
            // ordening (assume hilbert)
            string ordening = "hilbert";
            string ord_string(ordening);
            //cout << "ord_string " << ord_string << "\n";
            if ((ord_string.substr(0,9))=="bisection") {
                for (int i=0;i<5;i++){
                    skipRecord(file_amr);
                }
            } else {
                skipRecord(file_amr);
            }
            // skip course lvl: son, flag1, cpu_map
            skipRecord(file_amr);
            skipRecord(file_amr);
            skipRecord(file_amr);

            // HYRDO FILE

            // open hydro file
            string name_hydro_file = construct_name(icpu, output_dir, "hydro");
            ifstream file_hydro;
            file_hydro.open(name_hydro_file.c_str(), ios::in | ios::binary);
            if (!file_hydro.is_open()) {
                cout << "Could not open the hydro data " << name_hydro_file;
                return NULL;
            }
            // skip header: ncpu,nvarh,ndim,nlvlmax,nboundary,gamma
            skipRecord(file_hydro);
            readInt(file_hydro, nvarh);
            for (int p=0;p<4;p++){
                skipRecord(file_hydro);
            }

            // read fine levels, skip previous lvls
            for (int ilevel=0;ilevel<tree_lvl;ilevel++){
                //cout << "skipping lvl " << ilevel+1 << "\n";
                for (int ibound=0;ibound<(nboundary+ncpu);ibound++){
                    // skip amr data
                    if (ngridfile[ibound][ilevel]>0){
                        // skip: grid index, next index, prev index
                        skipRecord(file_amr);
                        skipRecord(file_amr);
                        skipRecord(file_amr);
                        // skip grid center
                        for (int idim=0; idim<ndim;idim++){
                            skipRecord(file_amr);
                        }
                        // skip father index
                        skipRecord(file_amr);
                        // skip nbor index, son index, cpu map, refinement map
                        for (int ind=0;ind<(2*ndim);ind++){
                            skipRecord(file_amr);
                        }
                        for (int ind=0;ind<twotondim;ind++){
                            skipRecord(file_amr);
                        }
                        for (int ind=0;ind<twotondim;ind++){
                            skipRecord(file_amr);
                        }
                        for (int ind=0;ind<twotondim;ind++){
                            skipRecord(file_amr);
                        }
                    }
                    // SKIP HYDRO DATA
                    skipRecord(file_hydro);//ilevel
                    skipRecord(file_hydro);//ncache
                    if (ngridfile[ibound][ilevel]>0){
                        // loop over cells in oct
                        for (int ind=0;ind<twotondim;ind++){
                            // skip all variables
                            for (int ivar=0; ivar<nvarh;ivar++){
                                skipRecord(file_hydro);
                            }
                        }
                    }
                }
            }

            // only read current lvl
            //cout << "reading level " <<  tree_lvl+1 << "\n";
            //geometry
            int ix,iy,iz;
            double dx = pow(0.5,tree_lvl+1);
            for (int ind=0;ind<twotondim;ind++) {
                iz=(ind)/4;
                iy=(ind-4*iz)/2;
                ix=(ind-2*iy-4*iz);
                xc[ind][0]=(double(ix)-0.5)*dx;
                xc[ind][1]=(double(iy)-0.5)*dx;
                xc[ind][2]=(double(iz)-0.5)*dx;
            }

            // Allocate work arrays
            int ngrida = ngridfile[icpu][tree_lvl];
            double** xg = new double*[ngrida]; //gridcenters
            double** var = new double*[ngrida];
            double** x = new double*[ngrida];
            for (int a=0;a<ngrida;a++){
                xg[a] = new double[ndim];
                var[a] = new double[twotondim];
                x[a] = new double[ndim];
            }

            // loop over domains
            for (int j=0;j<(nboundary+ncpu);j++) {
                // READ AMR DATA
                if (ngridfile[j][tree_lvl]>0){
                    // skip grid index, next index, prev index
                    skipRecord(file_amr);
                    skipRecord(file_amr);
                    skipRecord(file_amr);
                    // READ grid center
                    for(int idim=0;idim<ndim;idim++){
                        if(j==icpu){
                            // READ and store
                            int record_size=0;
                            file_amr.read((char*) &record_size, RECORD_DELIMITER_LENGTH);
                            if(record_size>0){
                                //cout << "dim " << idim << " num elements " << record_size/sizeof(double) << " ngrida " << ngrida << " ngridfile[j][tree_lvl] " << ngridfile[j][tree_lvl] << "\n";
                                for(int a=0; a<ngrida;a++) {
                                    file_amr.read((char*)&(xg[a][idim]), sizeof(double));
                                    //cout << "xg[" << a << "][" << idim << "] = " << xg[a][idim] << "\n";
                                }
                            }
                            file_amr.seekg(RECORD_DELIMITER_LENGTH, ios::cur);

                        } else {
                            skipRecord(file_amr);
                        }
                    }
                    // skip father index
                    skipRecord(file_amr);
                    // skip nbor index, son index, cpu map, refinement map
                    for (int ind=0;ind<(2*ndim);ind++){
                        skipRecord(file_amr);
                    }
                    for (int ind=0;ind<twotondim;ind++){
                        skipRecord(file_amr);
                    }
                    for (int ind=0;ind<twotondim;ind++){
                        skipRecord(file_amr);
                    }
                    for (int ind=0;ind<twotondim;ind++){
                        skipRecord(file_amr);
                    }
                }

                // READ HYDRO DATA
                skipRecord(file_hydro);//ilevel
                skipRecord(file_hydro);//ncache
                if (ngridfile[j][tree_lvl]>0){
                    //if(j==icpu){
                    //    cout << "reading HYDRO...\n";
                    //}
                    // loop over cells in oct
                    for (int ind=0;ind<twotondim;ind++){
                        for (int ivar=0; ivar<nvarh;ivar++){
                            // store rho
                            if((j==icpu) && (ivar==0)){
                                // READ and store
                                file_hydro.seekg(RECORD_DELIMITER_LENGTH, ios::cur);
                                // read the record:
                                for(int a=0; a<ngrida;a++) { // IS THIS THE RIGHT LOOP??
                                    file_hydro.read((char*)&(var[a][ind]), sizeof(double));
                                    if ((var[a][ind] < 1e-100) && (var[a][ind] > -1e-100)) {
                                        var[a][ind] = 0;
                                    }
                                }
                                file_hydro.seekg(RECORD_DELIMITER_LENGTH, ios::cur);

                            } else {
                                skipRecord(file_hydro);
                            }
                        }
                    }
                }
            }

            // ADD NODES THE TREE

            if (ngrida>0){
                //cout << "Adding " << ngrida << " octs...\n";
                for (int ind=0;ind<twotondim;ind++){ //loop over oct
                    for (int a=0;a<ngrida;a++){
                        //cout << "node " << ind << " of oct " << a << "\n";
                        //cout << "xc = " << xc[ind][0] << " " << xc[ind][1]<< " " << xc[ind][2] << "\n";
                        //cout << "xg = " << xg[a][0] << " " << xg[a][1]<< " " << xg[a][2] << "\n";
                        // calc cell center in box units
                        // xg = oct center
                        // xc is the offset off each cell
                        x[a][0]=xg[a][0]+xc[ind][0];//-xbound[0];
                        x[a][1]=xg[a][1]+xc[ind][1];//-xbound[1];
                        x[a][2]=xg[a][2]+xc[ind][2];//-xbound[2];
                        //cout << "position = " << x[a][0] << " " << x[a][1] << " " << x[a][2] <<"\n";

                        // search tree for the direct parent cell
                        // start from root
                        current_parent = root;
                        int branch_x=0;
                        int branch_y=0;
                        int branch_z=0;
                        int branch_index=0;
                        // go deeper into the tree lvl by lvl
                        for (int depth=0;depth<tree_lvl;depth++) {
                            // determine whether to go "left" or "right":
                            // compare position of node with positiion of current parent
                            branch_x = int(x[a][0]/current_parent->getX());
                            branch_y = int(x[a][1]/current_parent->getY());
                            branch_z = int(x[a][2]/current_parent->getZ());
                            //cout << "depth " << depth << "branch -> " << branch_x << " " << branch_y << " " << branch_z << "\n"; 
                            // move the parent pointer deeper into the tree
                            branch_index = branch_x + branch_y*2 + branch_z*4;
                            current_parent = current_parent->getChild(branch_index);
                        }
                        //cout << "test parent " << current_parent->getX() << " " <<current_parent->getY() << " " << current_parent->getZ() <<"\n";
                        // create the new node and add it to its parent
                        int index_child=ind;
                        Node* new_child = new Node(current_parent);
                        new_child->setLvl(tree_lvl+1);
                        new_child->setPos(x[a][0],x[a][1],x[a][2]);
                        new_child->setRho(var[a][ind]);
                        current_parent->addSon(new_child, index_child);
                    }
                }
            }

            // free work arrays
            for (int a=0;a<ngrida;a++){
                delete[] xg[a];
                delete[] var[a];
                delete[] x[a];
            }
            delete[] xg;
            delete[] var;
            delete[] x;

            file_amr.close();
            file_hydro.close();

        }
    }

    // free ngrid arrays
    for (int a=0;a<(ncpu+nboundary);a++){
        delete[] ngridfile[a];
    }
    delete[] ngridfile;

    cout << "Done loading.\n";

    return root;
}

/* Deallocate the tree */
void cleanup(Node* root){
    delete root;
}
