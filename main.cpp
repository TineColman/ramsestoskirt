/*
   RAMSES_TO_SKIRT
   Converts a RAMSES output to SKIRT input
   Author: Tine Colman
*/

#include <cstdlib>

#include "readAMR.h"

int main(int argc, char *argv[]) {

    /* Arguments:
        - the path to the RAMSES snapshot
        - filename for the resulting file to be fed into SKIRT
        - maximum level of refinement to be included */
    std::string output_dir;
    std::string filename;
    int max_lvl;
    if(argc == 4) {
        output_dir = argv[1];
        filename = argv[2];
        max_lvl = atoi(argv[3]);
    } else {
        output_dir = "output_00001";
        filename = "grid.txt";
        max_lvl = 8;
    }

    Node* root = load_ramses(output_dir, max_lvl);
    write_grid(root, max_lvl, filename);

    cleanup(root);

    return 0;
}
